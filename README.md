Для запуска приложения требуется Maven. Вы просто заходите в директорию проекта и набираете 
mvn tomcat:run -e

http://localhost:8080/jetbilling/jetbillingservice/main/persons


Requests for answers test from POSTMAN:

POST: http://localhost:8080/jetbilling/jetbillingservice/main/persons/add
firstName
lastName
money

GET: http://localhost:8080/jetbilling/jetbillingservice/main/persons/edit?id=2

GET: http://localhost:8080/jetbilling/jetbillingservice/main/persons/add

GET: http://localhost:8080/jetbilling/jetbillingservice/main/persons

TODO
1. Не коммитятся транзакции в БД (разбираюсь)
2. Если будут коммититься - что будет с автогенерацией ID при перезапуске сервера приложения?
3. Что такое  org.springframework.ui.Model и зачем туда добавлять
4. Автогенерация дат добавления и апдейта
5. Поля основных объектов
6. Запросы, связь 1:М в HIBERNATE (incl)
http://www.mkyong.com/hibernate/hibernate-one-to-many-relationship-example-annotation/
7. Будут ли на сервисе вставки, в т.ч. 1:М
8. Что возвращаем при вставке/редактировании/удалении?
9. Декомпозиция контроллера