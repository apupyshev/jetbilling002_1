package jetbillingservice.controller;

import java.util.List;

import javax.annotation.Resource;

import jetbillingservice.model.OperatorAdmin;
import jetbillingservice.model.Person;
import jetbillingservice.service.OperatorAdminService;
import jetbillingservice.service.PersonService;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;


@Controller
@RequestMapping("/main")
public class MainController {
	
    private static Logger logger = Logger.getLogger("controller");
    
	/* ====================== OPERATOR ADMIN =========================== */
    
    @Resource(name="operatorAdminService")
    private OperatorAdminService operatorAdminService;
    
    @RequestMapping(value = "/operatorAdmins", method = RequestMethod.GET)
    public @ResponseBody List<OperatorAdmin> getOperatorAdmins(Model model) {
    	//http://localhost:8080/jetbilling/jetbillingservice/main/operatorAdmins
    	
        logger.debug("Received request to show all operators-admins");

        // Retrieve all operatorAdmins by delegating the call to OperatorAdminService
        List<OperatorAdmin> operatorAdmins = operatorAdminService.getAll();

        // Attach operatorAdmins to the Model
        model.addAttribute("operatorAdmins", operatorAdmins);

        // This will return JSON-object
        return operatorAdmins;
    }
    
    /* ��������� ������ ��������� �� ID  [GET] .../OperatorAdmin/id */ 
    @RequestMapping(value = "/operatorAdmins/id", method = RequestMethod.GET)
    public @ResponseBody OperatorAdmin getOperatorAdmin(
    		@RequestParam(value="id", required=true) Integer id, Model model) {
    	//http://localhost:8080/jetbilling/jetbillingservice/main/operatorAdmins/id
    	
        logger.debug("Received request to show one operator-admin by id");

        // Retrieve all operatorAdmins by delegating the call to OperatorAdminService
        OperatorAdmin operatorAdmin = operatorAdminService.get(id);

        // Attach operatorAdmins to the Model
        model.addAttribute("operatorAdmin", operatorAdmin);

        // This will return JSON-object
        return operatorAdmin;
    }
    
    /* ��������� ������ ���� �������� ��������� �� ��� ID  [GET] .../OperatorAdmin/id/Operations */
       
    /* ��������� ������ ���� ���������� �������� ��������� �� ��� ID 
     * (���� �������� ������ ������) [GET] .../OperatorAdmin/id/Operation?isActual=true */
      
    /* ��������� ������ ���� ���������� ��������� �� ��� ID  [GET] .../OperatorAdmin/id/Transactions */   

    /* [GET] .../OperatorAdmin/id/CurrentStatus */
       
    /* [POST] .../OperatorAdmin { name: ��, email:��,  } */
       
    /* [POST] .../OperatorAdmin/id/Operation */


    
	/* ====================== PERSON =========================== */
    
    @Resource(name="personService")
    private PersonService personService;

    @RequestMapping(value = "/persons", method = RequestMethod.GET)
    public @ResponseBody List<Person> getPersons(Model model) {

        logger.debug("Received request to show all persons");

        // Retrieve all persons by delegating the call to PersonService
        List<Person> persons = personService.getAll();

        // Attach persons to the Model
        model.addAttribute("persons", persons);

        // This will return JSON-object
        return persons;
    }
    

    @RequestMapping(value = "/persons/add", method = RequestMethod.GET)
    public @ResponseBody String getAdd(Model model) {
        logger.debug("Received request to show add page");

        // Create new Person and add to model
        // This is the formBackingOBject
        model.addAttribute("personAttribute", new Person());

        // empty JSON emulate
        return "{}";
    }

    /**
     * Adds a new person by delegating the processing to PersonService.
     * Displays a confirmation JSP page
     * @return  the name of the JSP page
     */
    @RequestMapping(value = "/persons/add", method = RequestMethod.POST)
    public String add(@ModelAttribute("personAttribute") Person person) {
        logger.debug("Received request to add new person");

        // The "personAttribute" model has been passed to the controller from the JSP
        // We use the name "personAttribute" because the JSP uses that name
        // Call PersonService to do the actual adding
        personService.add(person);

        // empty JSON emulate
        return "{}";
    }

    /**
     * Deletes an existing person by delegating the processing to PersonService.
     * Displays a confirmation JSP page
     *
     * @return  the name of the JSP page
     */
    @RequestMapping(value = "/persons/delete", method = RequestMethod.GET)
    public String delete(@RequestParam(value="id", required=true) Integer id,
                         Model model) {

        logger.debug("Received request to delete existing person");

        // Call PersonService to do the actual deleting
        personService.delete(id);

        // Add id reference to Model
        model.addAttribute("id", id);

        // empty JSON emulate
        return "{}";
    }

    /**
     * Retrieves the edit page
     *
     * @return the name of the JSP page
     */
    @RequestMapping(value = "/persons/edit", method = RequestMethod.GET)
    public String getEdit(@RequestParam(value="id", required=true) Integer id,
                          Model model) {
        logger.debug("Received request to show edit page");

        // Retrieve existing Person and add to model
        // This is the formBackingOBject
        model.addAttribute("personAttribute", personService.get(id));

        // empty JSON emulate
        return "{}";
    }

    /**
     * Edits an existing person by delegating the processing to PersonService.
     * Displays a confirmation JSP page
     *
     * @return  the name of the JSP page
     */
    @RequestMapping(value = "/persons/edit", method = RequestMethod.POST)
    public String saveEdit(@ModelAttribute("personAttribute") Person person,
                           @RequestParam(value="id", required=true) Integer id,
                           Model model) {
        logger.debug("Received request to update person");

        // The "personAttribute" model has been passed to the controller from the JSP
        // We use the name "personAttribute" because the JSP uses that name

        // We manually assign the id because we disabled it in the JSP page
        // When a field is disabled it will not be included in the ModelAttribute
        person.setId(id);

        // Delegate to PersonService for editing
        personService.edit(person);

        // Add id reference to Model
        model.addAttribute("id", id);

        // empty JSON emulate
        return "{}";
    }

}