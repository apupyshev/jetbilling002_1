package jetbillingservice.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "OPERATORADMIN")
public class OperatorAdmin  /* TODO extends AbstractTimestampEntity */  implements Serializable {
	
  private static final long serialVersionUID = -55274534302296042L;

  @Id
  @Column(name = "ID")
  @GeneratedValue
  private Integer id;

  @Column(name = "FIRST_NAME")
  private String firstName;
   
  @Column(name = "LAST_NAME")
  private String lastName;
   
  @Column(name = "MONEY")
  private Double money;
  
  public String getFirstName() {
   return firstName;
  }

  public void setFirstName(String firstName) {
   this.firstName = firstName;
  }

  public static long getSerialVersionUID() {
   return serialVersionUID;
  }

  public Integer getId() {
   return id;
  }

  public void setId(Integer id) {
   this.id = id;
  }

  public String getLastName() {
   return lastName;
  }

  public void setLastName(String lastName) {
   this.lastName = lastName;
  }

  public Double getMoney() {
   return money;
  }

  public void setMoney(Double money) {
   this.money = money;
  }

}
